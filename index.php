<?php

	include('functions.php');
	$guild_level = db_guild_level();

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>TWDS - Guild status</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<link rel="stylesheet" href="bootstrap.css">
	<link rel="stylesheet" href="custom.css">
	<script src="bootstrap.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 vert-offset-top-4">
				<div class="row jumbotron padding-2">
					<div style="display: inline; float: left;">
						<img class="guild-logo" src="https://guilds.gw2w2w.com/guilds/thunder-wing-doom-slayers/150.svg"/>		
					</div>
					<div style="display: inline; float: left;">
						<div class="row">
							<div class="col-md-12">
								<h2>Thunder Wing Doom Slayers [TWDS]</h2>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-1 text-right padding-2">
									<strong><?=$guild_level['current_level']?></strong>
								</div>
								<div class="col-md-10 padding-2">
									<div class="progress">
										<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?=$guild_level['progress']?>%;">
											<strong><?=$guild_level['progress']?>%</strong>
										</div>
									</div>
								</div>
								<div class="col-md-1 text-left padding-2">
									<strong><?=$guild_level['next_level']?></strong>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">

					<?php

						db_view_upgrades();

					?>

				</div>

			</div>
		</div>
	</div>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-78780264-1', 'auto');
		ga('send', 'pageview');
	</script>
</body>
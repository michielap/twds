<?php

include('config.php');

function api_get_upgrades($upgrade_id = FALSE){
	//Get all possible upgrades if no $upgrade_id is specified.
	//Get information about upgrade if a $upgrade_id is given.

	$api_url = 'https://api.guildwars2.com/v2/guild/upgrades';
	if($upgrade_id !== FALSE){
		$api_url = $api_url . '/' . $upgrade_id;
	}
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $api_url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$curl_response 	= curl_exec($curl);
	if($curl_response === FALSE){
		$info = curl_getinfo($curl);
		curl_close($curl);
		die('error occured during curl execution. Additioanl info: ' . var_export($info));
	}
	curl_close($curl);
	$decoded = json_decode($curl_response);

	return $decoded;
}

function api_save_upgrades(){
	//First get list of all upgrades using api_upgrades().
	//Second get details of upgrade using api_upgrades($upgrde_id) and save it to db.
	global $pdo;

	$query = $pdo->prepare('TRUNCATE TABLE guild_upgrades_prerequisites');
	$query->execute();

	$upgrades = api_get_upgrades();
	foreach ($upgrades as $upgrade_id) {
		$details = api_get_upgrades($upgrade_id);
		$query = $pdo->prepare('INSERT INTO guild_upgrades
									(id, name, description, build_time, icon, type_id, required_level, experience)
								VALUES
									(:id, :name, :description, :build_time, :icon, (SELECT id FROM guild_upgrades_type WHERE type = :type), :required_level, :experience)
								ON DUPLICATE KEY UPDATE
									id = VALUES(id),
									name = VALUES(name),
									description = VALUES(description),
									build_time = VALUES(build_time),
									icon = VALUES(icon),
									type_id = VALUES(type_id),
									required_level = VALUES(required_level),
									experience = VALUES(experience)');
		$query->execute(array(':id' => $details->id, ':name' => $details->name, ':description' => $details->description, ':build_time' => $details->build_time, ':icon' => $details->icon, ':type' => $details->type, ':required_level' => $details->required_level, ':experience' => $details->experience));

		foreach ($details->prerequisites as $entry) {
			$query = $pdo->prepare('INSERT INTO guild_upgrades_prerequisites
										(upgrade_id, prerequisite)
									VALUES
										(:upgrade_id, :prerequisite)');
			$query->execute(array(':upgrade_id' => $details->id, ':prerequisite' => $entry));
		}
	}
}

function api_upgrade_status(){
	global $pdo;
	global $api_key;
	global $guild_key;

	$api_url = 'https://api.guildwars2.com/v2/guild/' . $guild_key . '/upgrades';
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $api_url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $api_key));
	$curl_response 	= curl_exec($curl);
	if($curl_response === FALSE){
		$info = curl_getinfo($curl);
		curl_close($curl);
		die('error occured during curl execution. Additioanl info: ' . var_export($info));
	}
	curl_close($curl);
	$decoded = json_decode($curl_response);

	var_dump($decoded);

	$query = $pdo->prepare('SELECT id FROM guild_upgrades');
	$query->execute();
	$upgrades = $query->fetchALL();

	foreach ($upgrades as $entry) {
		in_array($entry['id'], $decoded) ? $completed = 1 : $completed = 0;
		$query = $pdo->prepare('INSERT INTO guild_upgrades_completed
									(id, completed)
								VALUES
									(:id, :completed)
								ON DUPLICATE KEY UPDATE
									id = VALUES(id),
									completed = VALUES(completed)');
		$query->execute(array(':id' => $entry['id'], ':completed' => $completed));
	}

}

function db_icons(){
	global $pdo;

	$query = $pdo->prepare('SELECT id, icon FROM guild_upgrades');
	$query->execute();

	return $query->fetchALL(PDO::FETCH_KEY_PAIR);
}

function db_upgrade_status($cat_id = FALSE){
	global $pdo;

	if($cat_id === FALSE){
		$query = $pdo->prepare('SELECT * FROM guild_upgrades_completed)');
	}
	elseif(is_numeric($cat_id)){
		$query = $pdo->prepare('SELECT * FROM guild_upgrades_completed WHERE id IN (SELECT id FROM guild_upgrades WHERE cat_id = :cat_id) ORDER BY completed DESC');
		$query->bindParam(':cat_id', $cat_id);
	}

	$query->execute();

	return $query->fetchALL();
}

function db_upgrade_name(){
	global $pdo;

	$query = $pdo->prepare('SELECT id, name FROM guild_upgrades');
	$query->execute();

	return $query->fetchALL(PDO::FETCH_KEY_PAIR);
}

function api_download_images(){
	global $pdo;

	$icons = db_icons();

	foreach ($icons as $entry){
		$url = $entry;
		echo $url;
		$img = 'img/upgrades/' . end(explode('/', $entry));
		echo $img;
		file_put_contents($img, file_get_contents($url));
	}

}

function db_guild_level(){
	global $pdo;

	$query = $pdo->prepare('SELECT SUM(experience)
							FROM guild_upgrades
							WHERE id
							IN (
								SELECT id
								FROM guild_upgrades_completed
								WHERE completed = 1)');
	$query->execute();

	$gxp = $query->fetch(PDO::FETCH_ASSOC);
	$gxp = $gxp['SUM(experience)'];
	$current_level = floor($gxp / 100);
	$next_level = ceil($gxp / 100);
	$next_gxp = $next_level * 100;
	$progress = round( ($gxp - $current_level * 100) , 0);

	return array(
		'gxp' 			=> intval($gxp),
		'current_level' => intval($current_level),
		'next_gxp'		=> intval($next_gxp),
		'next_level'	=> intval($next_level),
		'progress'		=> intval($progress));
}

function db_view_upgrades(){

	$icons = db_icons();
	$upgrade_status = array(
						'notary' 	=> db_upgrade_status('1'),
						'arena' 	=> db_upgrade_status('2'),
						'market'	=> db_upgrade_status('3'),
						'mine'		=> db_upgrade_status('4'),
						'tavern'	=> db_upgrade_status('5'),
						'war_room'	=> db_upgrade_status('6'),
						'workshop'	=> db_upgrade_status('7')
					);
	$upgrade_name = db_upgrade_name();
	$categories = ['notary', 'arena', 'market', 'mine', 'tavern', 'war_room', 'workshop'];

	$i = 0;
	foreach ($categories as $entry){
		if ($i % 3 == 0){
			echo '</div><div class="row">';
		}

		echo '<div class="col-md-4">
				<img src="img/cat_icons/' . $entry . '.png" /><br />';
		foreach ($upgrade_status[$entry] as $upgrade) {
			$id = $upgrade['id'];
			$img =  end(explode('/', $icons[$id]));
			$style = $upgrade['completed'] == 1 ? '' : '-webkit-filter: grayscale(50%) opacity(40%); filter: grayscale(100%) opacity(40%);';
			echo '<img name="'.$id.'" id="'.$id.'" title="'.$upgrade_name[$id].'" style="'.$style.'" src="img/upgrades/'.$img.'">';
		}
		echo '</div>';
		$i += 1;
	}

	
}